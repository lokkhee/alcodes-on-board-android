package com.demoapp.alcodesonboard.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import com.afollestad.materialdialogs.MaterialDialog;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyNoteDetailActivity;
import com.demoapp.alcodesonboard.adapters.MyNotesAdapter;
import com.demoapp.alcodesonboard.database.entities.MyNote;
import com.demoapp.alcodesonboard.repositories.MyNotesRepository;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.demoapp.alcodesonboard.repositories.MyNotesRepository.getInstance;

public class MyNoteDetailFragment extends Fragment {

    public static final String TAG = MyNoteDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_NOTE_ID = "ARG_LONG_MY_NOTE_ID";

    @BindView(R.id.edittext_title)
    protected TextInputEditText mEditTextTitle;

    @BindView(R.id.edittext_content)
    protected TextInputEditText mEditTextContent;

    private Unbinder mUnbinder;
    private Long mMyNoteId = 0L;

    public MyNoteDetailFragment() {
    }

    public static MyNoteDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_NOTE_ID, id);

        MyNoteDetailFragment fragment = new MyNoteDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_note_detail, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            mMyNoteId = args.getLong(ARG_LONG_MY_NOTE_ID, 0);
        }

        initView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_my_note_detail, menu);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);

        // TODO change menu label "Save" to "Create" for new note. (line115)
        if(mMyNoteId == 0) menu.findItem(R.id.menu_save).setTitle("Create"); //Change the text to Create if there is no note
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_save) {
            // TODO show confirm dialog before continue. (line124)
            new MaterialDialog.Builder(getActivity())
                    .title("Confirmation")
                    .content("Do you want to save?")
                    .positiveText("Save")
                    .onPositive((dialog, which) -> {
                        // TODO check email and password is blank or not.(line 134)
                        String title = mEditTextTitle.getText().toString();
                        String content = mEditTextContent.getText().toString();

                        if(title.trim().isEmpty() || content.trim().isEmpty()){
                            new MaterialDialog.Builder(getActivity())
                                    .title("Empty Field")
                                    .content("Title and Content cannot be empty")
                                    .positiveText("OK")
                                    .show();

                            return;
                        }

                        // Save record and return to list.
                        MyNote myNote = new MyNote();
                        myNote.setTitle(title);
                        myNote.setContent(content);

                        // TODO BAD practice, should move Database operations to Repository.(line155&161)
                        if (mMyNoteId > 0) {
                            // Update record.
                            /*myNote.setId(mMyNoteId);

                            DatabaseHelper.getInstance(getActivity())
                                    .getMyNoteDao()
                                    .save(myNote);*/
                            getInstance().editNote(getActivity(), mMyNoteId, title, content);
                        } else {
                            // Create record.
                            /*DatabaseHelper.getInstance(getActivity())
                                    .getMyNoteDao()
                                    .insert(myNote);*/
                            getInstance().addNote(getActivity(), title, content);
                        }

                        Toast.makeText(getActivity(), "Note saved.", Toast.LENGTH_SHORT).show();

                        getActivity().setResult(MyNoteDetailActivity.RESULT_CONTENT_MODIFIED);
                        getActivity().finish();


                    })
                    .negativeText("Discard")
                    .onNegative((dialog, which) -> {
                        getActivity().setResult(MyNoteDetailActivity.RESULT_CANCELED);
                        getActivity().finish();
                    })
                    .show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        // TODO BAD practice, should move Database operations to Repository.(line190)
        if (mMyNoteId > 0) {
            /*MyNote myNote = DatabaseHelper.getInstance(getActivity())
                    .getMyNoteDao()
                    .load(mMyNoteId);*/

            MyNotesRepository loadData = MyNotesRepository.getInstance();
            loadData.loadMyNotesAdapterList(null);
            LiveData<List<MyNotesAdapter.DataHolder>> myNote = loadData.getMyNotesAdapterListLiveData();

            if (myNote != null) {
                myNote.observe(getActivity(), new Observer<List<MyNotesAdapter.DataHolder>>() {
                    @Override
                    public void onChanged(List<MyNotesAdapter.DataHolder> dataHolders) {
                        mEditTextTitle.setText(dataHolders.get(0).title);
                        mEditTextContent.setText(dataHolders.get(0).content);
                    }
                });
            } else {
                // Record not found.
                Toast.makeText(getActivity(), "Note not found.", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        }
    }
}
